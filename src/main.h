namespace ResPack
{
    enum ResPackID
    {
        ImageWidget1_Resources,
        ImageWidget2_Resources
    };
}

namespace Res
{
    enum ResID
    {
        lack_of_tests,
        trippy_back_rounds
    };
}

namespace Wdg 
{
    enum WidgetID
    {
        ImageWidget1,
        ImageWidget2
    };
};