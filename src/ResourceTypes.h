#ifndef RESOURCEIDS_H
#define RESOURCEIDS_H

namespace ResType
{
    enum ResTypeID
    {
        DefaultBitmap,
        DefaultText,
        DefaultFont
    };
}
#endif // RESOURCEIDS_H
